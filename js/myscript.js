$('#test').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    slideBy:2,
    navText:["<div class='nav-btn prev-slide'></div>","<div class='nav-btn next-slide'></div>"],
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
          }
        }
    })
    $('#new').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    slideBy:1,
    navText:["<div class='nav-btn2 prev-slidenew'></div>","<div class='nav-btn2 next-slidenew'></div>"],
    dots:false,
    responsive:{
       0:{
             items:1
         },
         600:{
             items:2
         },
         1000:{
             items:3
         }
       }
    })
    $('#new2').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    slideBy:1,
    navText:["<div class='nav-btn3 prev-slidenew'></div>","<div class='nav-btn3 next-slidenew'></div>"],
    dots:false,
    items:1
    })
    $('#btnJump').click(function(){
        $('#new2').trigger('to.owl.carousel', 0)
      });
    $('#btnJump1').click(function(){
    $('#new2').trigger('to.owl.carousel', 1)
        });
    $('#btnJump2').click(function(){
    $('#new2').trigger('to.owl.carousel', 2)
      });