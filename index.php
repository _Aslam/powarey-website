<?php

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbase = 'test';

   $dab = mysqli_connect($dbhost,$dbuser,$dbpass,$dbase);
  // Check connection
  if (mysqli_connect_errno())
    {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
   if($requestMethod == "POST")
   {
      echo "im alive";
   }
         
            //fetching data from view of project
            $sdetails ="SELECT * FROM projects";
            $sresult = mysqli_query($dab,$sdetails);
            $rows = mysqli_num_rows($sresult);
              
            if($sresult)
            {
              for ($j = 0 ; $j < $rows; $j++)
              {
                $row = mysqli_fetch_array($sresult,MYSQLI_ASSOC); 
              }  
            }

            //fetching data from view of about
            $details ="SELECT * FROM about";
            $dresult = mysqli_query($dab,$sdetails);
            $drows = mysqli_num_rows($dresult);
              
            if($dresult)
            {
              for ($j = 0 ; $j < $drows; $j++)
              {
                $row = mysqli_fetch_array($dresult,MYSQLI_ASSOC); 
              }  
            }

             //fetching data from view of project
            $cdetails ="SELECT * FROM client";
            $cresult = mysqli_query($dab,$cdetails);
            $crows = mysqli_num_rows($cresult);
                          
            if($cresult)
            {
            for ($j = 0 ; $j < $crows; $j++)
            {
              $crow = mysqli_fetch_array($cresult,MYSQLI_ASSOC); 
            }  
            }

            //fetching services details
            $sdetails2 ="SELECT * FROM services";
            $sresult2 = mysqli_query($dab,$sdetails2);
            $rows2 = mysqli_num_rows($sresult2);
              
            if($sresult2)
            {
              for ($j = 0 ; $j < $rows2; $j++)
              {
                $row2 = mysqli_fetch_array($sresult2,MYSQLI_ASSOC); 
              }  
            }

            

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="images/path830.png" />
    <link rel="stylesheet" href="owl/owl.carousel.css">
    <link rel="stylesheet" href="owl/owl.theme.default.min.css">
    <link rel="stylesheet" href="grid_system.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="mystyle.css" rel="stylesheet">
    <title>PAWAREY</title>
</head>
<body>
        <section id="first-div">
            <div class="container"style="width:100%">
              <div class="row">
                <div class="col-12 col-md-5 text-white">
                  <img src="images/Ltter Head-01-01.png">
                  <a href="#home">Home</a>
                  <a href="#about">About</a>
                  <a href="#contact">Contact</a>
                  <h1 id="tag"><b>Leadig Metal</b></h1>
                  <h2 id="tag2">Fabricator of Maldives.</h2>
                  <a href="#" class="fa fa-email"></a>
                  <hr>
                </div>
                <div class="col-12 col-md-7" id="second-div">
                </div>
              </div>
            </div>    
          </section>
    <div class="wrapper2">
      <div class="container">
        <div class="row">
          <div id="slide" class="p-3">
              <h1 style="font-size: 58px;padding-top:50px"><b>Clients</b></h1>Some of our satisfied Customers  
          </div>
          <div style="width:50%;margin: 0;padding: 0;float: left;padding-top: 20px;padding-bottom: 20px;">
      
              <div class="carousel-wrap"style="width:840px">
                  <div class="owl-carousel owl-theme" id="test">
                  <?php
                $cresult = mysqli_query($dab,$cdetails);
                if($cresult){
                  for ($j = 0 ; $j < $crows; $j++){
     //               echo $crows;
                    $crow = mysqli_fetch_array($cresult,MYSQLI_ASSOC);
                    echo "<div class='item'>";
                    echo"<img src='".$crow['logo']."'/>";
                    echo"<span class='img-text'>".$crow['name']."</span>";
                    echo"</div>";
                }
            }?>
                  </div>
                </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrapper" id="about">
      <div class="container">
        <div class="row py-4">
          <div class="col-12 col-lg-6">
            <h1 style="color: rgb(233, 33, 62);font-size: 58px;">About Us</h1>
            <p><?php
                $dresult = mysqli_query($dab,$details);
                if($dresult){
                    $drow = mysqli_fetch_array($dresult,MYSQLI_ASSOC);
                    echo $drow['about'];
            }?></p>
          </div>
          <div class="col-12 col-lg-6 d-flex align-self-center">
            <div class="px-2">
              <img class="img-fluid" src="images/path828.png">
            </div>
            <div class="px-2">
              <img class="img-fluid" src="images/path826.png">
            </div>
          </div>
        </div> 
      </div>
    </div>
    <div class="container py-4">
      <div class="row">
        <div id="slideshow">
                <h1 style="color: rgb(233, 33, 62);font-size: 58px;"> Our Latest projects</h1>
                <div class="carousel-wrapnew">
                  <div class="owl-carousel parent-box" id="new">
                  <?php
                $sresult = mysqli_query($dab,$sdetails);
                if($sresult){
                  for ($j = 0 ; $j < $rows; $j++){
                    $row = mysqli_fetch_array($sresult,MYSQLI_ASSOC);
                    echo "<div class='box'>";
                    echo"<img src='".$row['images']."'style='height:80%'/>";
                    echo"<span class='img-text'style='font-size:30px'>".$row['discription']."</span>";
                    echo"</div>";
                }
            }?>
                  </div>
                </div>
        </div>
      </div>
    </div>
    <div id="services">

        <div class="container">

            <div id="s1"><h1 style="font-size: 50px;margin-top: 0;">our offered services</h1>
            <br><h3 style="font-size:30px" id="btnJump">Construction</h3><br><h3 style="font-size: 30px;" id="btnJump1">Tanks Building</h3>
            <br><h3 style="font-size:30px" id="btnJump2">Barge Building</h3>
            </div>
            <div id="s2">
              <div class="carousel-wrapnew2">
                <div class="owl-carousel parent-box" id="new2">
                <?php
                $sresult2 = mysqli_query($dab,$sdetails2);
                if($sresult2){
                $j=0;
                while($j<$rows2){
                    echo"<div class='box2'>";
                    $c=$j+9;
                    while($j<$c && $j<$rows2){
                      echo"<div class='flex-container'>";
                      $k=$j+3;
                      while($j<$k && $j<$rows2){
                        $row2 = mysqli_fetch_array($sresult2,MYSQLI_ASSOC);
                        echo "<div>";
                        echo"<img src='".$row2['image']."' alt='".$row2['image']."'style='height:80%'/>";
                        echo"<span class='img-text'>".$row2['name']."</span>";
                        echo"</div>";
                        $j=$j+1;
                      }
                      echo"</div>";
                    }
                    echo"</div>";
                }
              }
                    ?>
                </div>
              </div>
            </div>

        </div>

    </div>
    <div id="contact">
        <div id="c1">
            <h2 style="font-size: 45px bold;">General inquiries</h2>
            <br><br>
            Pawarey Investment Private Limited
            <br>Thilafushi, 179 Male.Rep of Maldives.<br><br><br>
            Tel +(960)9665549<br>
            Email: ali@Pawarey.com
            <br><br><br><br>
            <h1>Got Questions?</h1><br><br><br><br><br>
            <a href="#" class="fa fa-facebook"></a>
            <a href="#" class="fa fa-instagram"></a>
            <a href="#" class="fa fa-whatsapp"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class="fa fa-phone"></a>
  
        </div>
        <div id="c2">
            <h1 style="font-size: 58px;color: rgb(233, 33, 62);font-style: strong;">Get in Touch</h1>
            <form action="php/contact.php" method="POST">
                <label for="name">Name</label><br><br>
                <input type="text" name="fname" placeholder="first name*" required> &nbsp; <input type="text" name="lname" placeholder="last name">
                <br><br>
                
                <label for="email">Email*</label><br>
                <input type="email" name='email' required><br><br>
                <label for="comment">Comment*</label><br><br>
                <textarea rows="10" cols="90" name="comment" required></textarea>
                <br><br>
                <input type="submit" value="Submit">
            </form>
      </div>
    </div>
    <div id="footer">Copyright &copy;2019 All rights reserved | <b style="color: rgb(233, 33, 62);">Pawarey investment pvt ltd</b></div>
    <script src="js/jquery.js"></script>
<script src="owl/owl.carousel.js"></script>
<script src="js/myscript.js"></script>
</body>
</html>
